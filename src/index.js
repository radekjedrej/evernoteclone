import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const firebase = require('firebase');
require('firebase/firestore');

firebase.initializeApp({
  apiKey: "AIzaSyDAcJH1w4F8RXo2MnaooRCYprywa4g4H3A",
  authDomain: "evernote-clone-master.firebaseapp.com",
  databaseURL: "https://evernote-clone-master.firebaseio.com",
  projectId: "evernote-clone-master",
  storageBucket: "evernote-clone-master.appspot.com",
  messagingSenderId: "197851975785",
  appId: "1:197851975785:web:6b892f8068e451544a12f8"
});

ReactDOM.render(<App />, document.getElementById('evernote-container'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
